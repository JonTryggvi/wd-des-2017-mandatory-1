$(document).ready(function() {

  var table = $('#authors').DataTable(
      {
        "bLengthChange": false,
        "language": {
          "search": "",
           "searchPlaceholder": "Who are you looking for?"
        },
        "pagingType": "numbers",
        "buttons": [
          'colvis',
          'excel',
          'print'
        ],
        "info": false

      }
    );
    var ajAuthors = [];

    $.getJSON('js/data.json', function(data){
      // console.log(data.authors);
      var ajAuth = data.authors;
      // console.log(ajAuth);
      for (var i = 0; i < ajAuth.length; i++) {
        ajAuthors.push(ajAuth[i]);
      }

      // console.log(data);
    });


  // console.log($(window).width());
  if($(window).width() > 768) {
    $('.btnAuthorExpand').click(function(){
        var sAuthorDetailes = '';
        var detailsContainer = $('.auth-details-container');
        var rowActiveClass = $('.activateAuthor');
        var btnExpander = $('.btnAuthorExpand');
        var grandParent = $(this).parent().parent();
        var authId = Number(grandParent.attr('data-id'));
        var articles = $(this).parent().siblings()[1].textContent;
        var language = $(this).parent().siblings()[5].textContent;
        var graph = $(this).parent().siblings()[2].textContent;
        var likes = $(this).parent().siblings()[3].textContent;
        var dislikes = $(this).parent().siblings()[4].textContent;
        var jDataAuth = ajAuthors;
        // console.log(ajAuthors);
        var authName = "";
        var authEmail = "";
        var authPosition = "";
        var authWork = "";
        var authTel = "";
        var img = "";
        var joined = "";
        var aAuthWork;
        var sTrAuthWork = "";
        for (var i = 0; i < jDataAuth.length; i++) {
          if (jDataAuth[i].id == authId) {
            authName = jDataAuth[i].name;
            authEmail = jDataAuth[i].email;
            authPosition = jDataAuth[i].position;
            authWork = jDataAuth[i].firm;
            authTel = jDataAuth[i].tel;
            authImg = jDataAuth[i].img;
            joined = jDataAuth[i].joined;
            aAuthWork = jDataAuth[i].inProgress;
            var sArtName = ""
            for (var p = 0; p < aAuthWork.length; p++) {
              // console.log(p);
              sArtName = aAuthWork[p].title;
              sDueDate = aAuthWork[p].deadline;
              sTrAuthWork += '<tr><th>'+sArtName+'</th><th>'+sDueDate+'</th></tr>';
            }
          }
        }
        // console.log(authName);


        sAuthorDetailes = '<div class="auth-details-container">\
                            <div class="info-container">\
                              <div class="info-container__statistics">\
                                <div class="statPanel articles">'+articles+'</div>\
                                <div class="statPanel active">'+joined+'</div>\
                                <div class="statPanel language">'+language+'</div>\
                                <div class="statPanel graphs">'+graph+'</div>\
                                <div class="statPanel likes">'+likes+'</div>\
                                <div class="statPanel dislikes">'+dislikes+'</div>\
                              </div>\
                              <div class="author-contact">\
                                <div class="info">\
                                  <div class="info__text">\
                                    <h2>'+authName+'</h2>\
                                    <a href="mailto:'+authEmail+'">'+authEmail+'</a>\
                                    <p>'+authPosition+'</p>\
                                    <p>'+authWork+'</p>\
                                    <a href="tel:'+authTel+'">'+authTel+'</a>\
                                  </div>\
                                  <div class="info__img" style="background-image:url('+authImg+')"></div>"\
                                </div>\
                                <div class="message-container">\
                                  <h2>Send message</h2>\
                                  <form>\
                                    <textarea></textarea>\
                                    <button class="btn" type="button">send</button>\
                                  </form>\
                                </div>\
                              </div>\
                            </div>\
                            <div class="status-container">\
                              <div class="wrapper">\
                                <h2>Working on:</h2>\
                                <table>\
                                  <thead>\
                                    <tr>\
                                      <th>Article title</th><th>Deadline</th>\
                                    </tr>\
                                  </thead>\
                                  <tbody>\
                                    '+sTrAuthWork+'\
                                  </tbody>\
                                </table>\
                              </div>\
                            </div>\
                          </div>';

        $(this).toggleClass('btnAccordActive');
        grandParent.toggleClass('activateAuthor');
        grandParent.append(sAuthorDetailes);

    });
  } else if ($(window).width() < 768) {
    $('.btnAuthorExpand').addClass('btn');
  }





});
