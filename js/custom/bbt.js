//Sidebar
var selector = '.btnSidebar';
var iconSelector = '.sidebarActive_';
var aDataIcon = [];
$(selector).on('click', function(){
    var dataIcon = $(this).attr('data-icon');
    var selLoop = $(selector);
    // console.log(selLoop);
    for (var i = 0; i < selLoop.length; i++) {
      // console.dir(selLoop[i]);
      if(selLoop[i].dataset.icon != dataIcon) {
        selLoop[i].classList.remove('sidebarActive_'+selLoop[i].dataset.icon);
      }
    }
    $(selector).removeClass('sidebarActive');
    $(this).addClass('sidebarActive');
    $(this).addClass('sidebarActive_' + dataIcon);

});

//toggle slide up week statistics
var btnWeekStatistics = '.btnWeekStatistics';
var classWeekStatistics = document.querySelector(".dashboard__container__weekStatistics");
var classWeekStatisticsText = document.querySelector(".btnWeekStatistics_text");


$(btnWeekStatistics).click(function(){
  if (this.classList.contains( 'btnWeekStatistics_hide' )) {
    //console.log("yes it dos");
    $(classWeekStatistics).slideToggle();
    $(this).removeClass('btnWeekStatistics_hide');
    $(classWeekStatisticsText).removeClass('btnWeekStatistics_text_show');
  } else {
    $(classWeekStatistics).slideToggle();
    $(this).addClass('btnWeekStatistics_hide');
    $(classWeekStatisticsText).addClass('btnWeekStatistics_text_show');
  }

});

//graph from chart.js for
var myChart = document.getElementById("myChart");
if(myChart) {
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: ["Energy", "BYN", "Nordics", "Oil", "Rates", "Majors"],
          datasets: [{
              data: [18, 28, 5, 12, 8, 15],
              backgroundColor: [
                  '#F1F2F5',
                  '#4EC9B0',
                  '#F1F2F5',
                  '#F1F2F5',
                  '#F1F2F5',
                  '#F1F2F5'
              ],
              borderWidth: 0
          }]
      },
      options: {
          legend:{display: false},
            scales: {
                yAxes: [{
                    ticks: {
                      display: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                      display: false
                    }
                }]
            }
      }
  });
}


//graph from chart.js for Article performance
var myLineChart2 = document.getElementById("myLineChart2");
if(myLineChart2) {
  var ctx = document.getElementById("myLineChart2").getContext('2d');
  var myLineChart2 = new Chart(ctx, {
      type: 'line',
      data: {
          labels: ["Jun", "Jul", "Aug", "Sep", "Nov", "Dec"],
          datasets: [{
              label: [],
              data: [42, 20, 30, 66, 64, 30],
              backgroundColor: [
                  '#FDECE4'
              ],
              borderColor: [
                  '#FF6565'
              ],
              borderWidth: 2
          }]
      },
      options: {
        legend:{display: false},
          elements: {
            point: {
              radius: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    max: 80
                },
                gridLines: {
                  display: false
                }
            }],
            xAxes: [{
                gridLines: {
                  display: false
                }
            }]
        }
      }
  });
}



//graph from chart.js for Click Rate
var myLineChart = document.getElementById("myLineChart");
if(myLineChart) {
  var ctx = document.getElementById("myLineChart").getContext('2d');
  var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: ["Researc", "Webinars", "Podcasts", "Graduates", "Insights", "Events"],
          datasets: [{
              label: '# of Votes',
              data: [12, 16, 10, 8, 10, 4],
              backgroundColor: [
                  '#ffffff'
              ],
              borderColor: [
                  '#FF6565'

              ],
              borderWidth: 2
          }]
      },
      options: {
          legend:{display: false},
            elements: {
              line: {tension: 0}
          },
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      max: 20
                  },
                  gridLines: {
                    display: false
                  }
              }],
              xAxes: [{
                  gridLines: {
                    display: false
                  }
              }]
          }
      }
  });
}
