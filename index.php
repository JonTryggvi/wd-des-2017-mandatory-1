<?php
  session_start();
  $loggedIn = $_SESSION['loggedIn'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Nordea editor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    <link rel="stylesheet" href="css/styles.css">
  </head>
  <body>
    <?php if(!$loggedIn): ?>
    <?php require_once('partials/login.php'); ?>
    <?php else: ?>
    <header class="header">
      <?php require_once('partials/header-nav.php'); ?>
    </header>
    <div class="body-container">
      <section id="sidebar" class="body-container__sidebar-container">
        <div class="body-container__sidebar-container__mobile-nav-container">
          <div id="btnHamburger" class="body-container__sidebar-container__mobile-nav-container__burger ">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>

          <?php require_once('partials/sidebar.php'); ?>
      </section>
      <section id="view" class="body-container__view-container">
        <?php require_once('partials/dashboard.php'); ?>
        <?php require_once('partials/graph.php'); ?>
        <?php require_once('partials/heatmap.php'); ?>
        <?php require_once('partials/authors.php'); ?>
        <?php require_once('partials/trends.php'); ?>
        
        <?php require_once('partials/profile.php'); ?>
      </section>
    </div>
    <?php endif; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
  </body>
</html>
