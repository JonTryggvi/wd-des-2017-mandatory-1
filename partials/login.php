<div class="body-container__view-container__login-container">
  <a class="login__logo__anchor" href="#">
    <img src="icons/logo-light.png" alt="logo">
  </a>
  <div class="login__box">
    <h2>Sign in as</h2>
    <div class="btnLoginContainer">
      <button class="btnSignInAs author" id="author" type="button" name="button">Author</button>
      <button class="btnSignInAs editor" id="editor" type="button" name="button">Editor</button>
    </div>
    <div class="login__box__message hiddenMessage" id="message">
      <p>Hello <span id="signedInAs"></span>! <br>Please fill out the form below to get started</p>
    </div>
    <form class="login__box__form" action="index.html" method="post">
        <label class="login__box__form__input" for="">
          <input type="text" name="" value="" placeholder="Email">
          <input type="password" name="" value="" placeholder="Password">
          <button class="btnForgotPassword" type="button" name="button">Forgot?</button>
        </label>
      <div class="login__box__form__btn">
        <h3>No account? <span> Sign Up</span></h3>
        <button id="btnLogin" class="btn" type="button" name="button">Login</button>
      </div>
    </form>
  </div>
</div>
