<nav class="header__nav wrapper">
  <h1 class="header__nav__logo">
    <a class="header__nav__logo__anchor" href="#">
      <img src="icons/logo-light.png" alt="Nordea editor home">
    </a>
  </h1>
  <div id="userDropdown" class="header__nav__user">
    <span>Tina S.
        <div class="header__nav__user__dropdown">
          <a class="btnSidebar" href="#" data-showpage="pageProfile">My Profile</a>
          <a id="btnLogOut" href="#">Log Out</a>
        </div>
    </span>
    <img src="img/tina2x.png" width="50" height="50" alt="user img">
  </div>
</nav>
