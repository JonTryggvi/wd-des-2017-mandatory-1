<div class="body-container__sidebar-container__sidebar ">

  <a data-icon="dash" class="body-container__sidebar-container__sidebar__dash btnSidebar sidebarActive sidebarActive_dash" href="#" data-showpage="pageDashboard">Dashboard</a>
  <a data-icon="graph" class="body-container__sidebar-container__sidebar__graph btnSidebar" href="#" data-showpage="pageGraph">Graphs</a>
  <a data-icon="heat" class="body-container__sidebar-container__sidebar__heat btnSidebar" href="#" data-showpage="pageHeat">Heatmap</a>
  <a data-icon="auth" class="body-container__sidebar-container__sidebar__auth btnSidebar" href="#" data-showpage="pageAuthors">Authors</a>
  <a data-icon="trend" class="body-container__sidebar-container__sidebar__trend btnSidebar" href="#"data-showpage="pageTrend">Trends</a>
</div>
