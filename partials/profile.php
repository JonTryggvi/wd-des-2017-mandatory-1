<div id="pageProfile" class="profile page">
 <div class="profile__container">
   <div class="profile__container__topBar">
     <div class="profile_image">
        <img src="img/tina2x.png" alt="">
        <div class="img_overlay">
          <span><img src="icons/edit.svg" alt=""></span>
        </div>
     </div>
     <h2>Tina Sofie Jespersen - <span>Senior Editor <img class="editIcon" src="icons/edit.svg" alt=""></span></h2>
   </div>
   <div class="profile__container__profile">
     <div class="profile__container__profileInfo">
       <div class="profile__container__profileInfo__email">
         <h3>tinastinejespersen@nordea_editor.dk</h3>
       </div>
       <div class="profile__container__profileInfo__password">
         <h3>Password: **********</h3>
       </div>
       <div class="profile__container__profileInfo__phone">
         <h3>tel. +45 93845259</h3>
       </div>
     </div>
     <div class="profile__container__profileEdit">
       <span><img class="editIcon" src="icons/edit_green.svg" alt=""> Edit email</span>
       <span><img class="editIcon" src="icons/edit_green.svg" alt=""> Edit password</span>
       <span><img class="editIcon" src="icons/edit_green.svg" alt=""> Edit Phonenumber</span>
     </div>
    </div>
    <div class="profile__container__profileBio">
      <div class="profile__container__profileBio__text">
        <h3>Biography</h3>
        <p>Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.<br>
        Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.</p>
      </div>
      <div class="profile__container__profileBio__edit">
        <span><img class="editIcon" src="icons/edit_green.svg" alt=""> Edit Biography</span>
      </div>
    </div>

 </div>
</div>
