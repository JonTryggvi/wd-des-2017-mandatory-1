<div id="pageDashboard" class="dashboard__container page showPage">
  <div class="dashboard__container__weekStatistics">
    <div class="dashboard__container__weekStatistics__content">
      <img src="icons/document_black.svg" alt="">
      <span>46</span>
      <p>Articles</p>
    </div>
    <div class="dashboard__container__weekStatistics__content">
      <img src="icons/like.svg" alt="">
      <span>375</span>
      <p>Likes</p>
    </div>
    <div class="dashboard__container__weekStatistics__content">
      <img src="icons/message.svg" alt="">
      <span>56</span>
      <p>Comments</p>
    </div>
    <div class="dashboard__container__weekStatistics__content">
      <img src="icons/user.svg" alt="">
      <span>12</span>
      <p>Active</p>
    </div>
    <div class="dashboard__container__weekStatistics__content">
      <img src="icons/share.svg" alt="">
      <span>34</span>
      <p>Shares</p>
    </div>
  </div>
  <div class="btnWeekStatistics_text">
    <h3>This week statistics</h3>
  </div>
  <div class="btnWeekStatistics_line"> <button class="btnWeekStatistics" id="btnWeekStatistics" type="button" name="button"></button>
</div>

  <div class="dashboard__container__headlineFilter">
    <div class="dashboard__container__headlineFilter__text">
      <h2>View of Article Statistics</h2>
    </div>
    <div class="dashboard__container__headlineFilter__filter">
      <div class="filterButton">
        <p>30 days</p>
        <p>60 days</p>
        <p>90 days</p>
      </div>
      <div class="filterButton_top">
        <p>30 days</p>
      </div>
    </div>
  </div>
  <div class="dashboard__container__mainDash">
    <div class="leftColumn">
      <div class="card articlePerformance">
        <div class="cardHeader">
          <h3>Article Performance</h3>
        </div>
        <div class="articlePerformance__container">
            <p>Written articles from June to December</p>
          <canvas  id="myLineChart2" width="420" height="210" style="margin: 0 auto;"></canvas>
        </div>
      </div>
      <div class="card clickRate">
        <div class="cardHeader">
          <h3>Click Rate</h3>
        </div>
        <div class="clickRate__container">
          <p>Monday 4th of November</p>
          <canvas  id="myLineChart" width="480" height="280" style="margin: 0 auto;"></canvas>
        </div>

      </div>
    </div>
    <div class="rightColumn">
      <div class="card authors">
        <div class="cardHeader">
          <h3>Authors</h3>
          <div class="authorTableIcons">
            <img src="icons/graphs.svg" alt="">
            <img src="icons/document.svg" alt="">
            <img src="icons/like_grey.svg" alt="">
          </div>
        </div>
        <div class="authors__cardContent">
          <div class="authors__cardContent__first">
            <div class="authors__cardContent__first__picName">
              <img src="img/JonTryggvi.png" alt="">
              <p>Jón Tryggvi</p>
            </div>
            <div class="authors__cardContent__first__numbers">
              <span>34</span>
              <span>234</span>
              <span>12</span>
            </div>
          </div>

          <div class="authors__cardContent__first">
            <div class="authors__cardContent__first__picName">
              <img src="img/Smith1.png" alt="">
              <p>Eyþór</p>
            </div>
            <div class="authors__cardContent__first__numbers">
              <span>78</span>
              <span>185</span>
              <span>9</span>
            </div>
          </div>

          <div class="authors__cardContent__first">
            <div class="authors__cardContent__first__picName">
              <img src="img/SaraJepsen.png" alt="">
              <p>Sara Aaroee</p>
            </div>
            <div class="authors__cardContent__first__numbers">
              <span>87</span>
              <span>145</span>
              <span>23</span>
            </div>
          </div>

          <div class="authors__cardContent__first">
            <div class="authors__cardContent__first__picName">
              <img src="img/BirnaTh.png" alt="">
              <p>Thorkelsdottir</p>
            </div>
            <div class="authors__cardContent__first__numbers">
              <span>32</span>
              <span>98</span>
              <span>43</span>
            </div>
          </div>

          <div class="authors__cardContent__first">
            <div class="authors__cardContent__first__picName">
              <img src="img/KatrinSig.png" alt="">
              <p>Katrin Dúa</p>
            </div>
            <div class="authors__cardContent__first__numbers">
              <span>55</span>
              <span>84</span>
              <span>5</span>
            </div>
          </div>

          <div class="authors__cardContent__first">
            <div class="authors__cardContent__first__picName">
              <img src="img/Smith2.png" alt="">
              <p>Smith Johnson</p>
            </div>
            <div class="authors__cardContent__first__numbers">
              <span>21</span>
              <span>32</span>
              <span>2</span>
            </div>
          </div>

        </div>
      </div>
      <div class="card trendingTopics">
        <div class="cardHeader">
          <h3>Trending Topics this Month</h3>
        </div>
        <div class="trendingTopics__container">
          <canvas id="myChart" width="480" height="180" style="margin: 0 auto;"></canvas>
        </div>
      </div>
    </div>
  </div>

  <div class="dashboard__container__secondDash">
    <h2>Most Popular Articles</h2>
    <div class="owl-carousel">
      <div>
        <div class="owl-item__image carousel_img1"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>Start dates for EU margin requirements.</h3>
            <p>July 30, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt="">Katrín Dúa</p>
            <p><img src="icons/like.svg" alt="">234</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img2"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>ESMA consults on transparency rules for...</h3>
            <p>July 3, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt="">Jón Tryggvi</p>
            <p><img src="icons/like.svg" alt="">233</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img3"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>The European Commission publish...</h3>
            <p>July 30, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt=""> Sara Aarøe</p>
            <p><img src="icons/like.svg" alt="">600</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img4"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>Three things a currency strategy should...</h3>
            <p>May 30, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt="">Birna Bryndís</p>
            <p><img src="icons/like.svg" alt="">24</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img1"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>Start dates for EU margin requirements.</h3>
            <p>July 30, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt="">Katrín Dúa</p>
            <p><img src="icons/like.svg" alt="">234</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img2"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>ESMA consults on transparency rules for...</h3>
            <p>July 3, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt="">Jón Tryggvi</p>
            <p><img src="icons/like.svg" alt="">233</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img3"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>The European Commission publish...</h3>
            <p>July 30, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt=""> Sara Aarøe</p>
            <p><img src="icons/like.svg" alt="">600</p>
          </div>
        </div>
      </div>
      <div>
        <div class="owl-item__image carousel_img4"></div>
        <div class="owl-item__info">
          <div class="owl-item__info__title">
            <h3>Three things a currency strategy should...</h3>
            <p>May 30, 2015</p>
          </div>
          <div class="owl-item__info__author">
            <p><img src="icons/user.svg" alt="">Birna Bryndís</p>
            <p><img src="icons/like.svg" alt="">24</p>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
