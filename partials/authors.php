<div id="pageAuthors" class="pageAuthors page">
  <div class="authors">
    <h2>Author List</h2>
    <table id="authors" class="display" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Authors</th>
          <th>Articles</th>
          <th>Graphs</th>
          <th>Likes</th>
          <th>Dislikes</th>
          <th>Language</th>
          <th>...</th>
        </tr>
      </thead>
        <tbody>
          <tr class="authRow" data-id="1" >
            <th><span><img src="img/JonTryggvi.png" alt=""></span>Jón Tryggvi Unnarsson</th>
            <th class="statistics article desk" data-article="355">355</th>
            <th class="statistics graphs desk" data-graphs="65" >65</th>
            <th class="statistics likes desk" data-likes="60">60</th>
            <th class="statistics unlikes desk" data-unlikes="5">5</th>
            <th class="statistics lang desk" data-lang="IS">IS</th>
            <th class="theExpander"><span class="btnAuthorExpand"></span></th>
            <!-- <th class="mobile"><button class="btn">Send message</button></th> -->
          </tr>
          <tr class="authRow" data-id="2" >
            <th><span><img src="img/SaraJepsen.png" alt=""></span>Sara Jepsen</th>
            <th class="statistics article desk" data-article="300">300</th>
            <th class="statistics graphs desk" data-graphs="80">80</th>
            <th class="statistics likes desk" data-likes="55">55</th>
            <th class="statistics unlikes desk" data-unlikes="8">8</th>
            <th class="statistics lang desk" data-lang="DK">DK</th>
            <th class="theExpander"><span class="btnAuthorExpand"></span></th>
          </tr>
          <tr class="authRow" data-id="3" >
            <th><span><img src="img/KatrinSig.png" alt=""></span>Katrín Sigurðardóttri</th>
            <th class="statistics article desk" data-article="250">250</th>
            <th class="statistics graphs desk" data-graphs="90">90</th>
            <th class="statistics likes desk" data-likes="76">76</th>
            <th class="statistics unlikes desk" data-unlikes="8">8</th>
            <th class="statistics lang desk" data-lang="IS">IS</th>
            <th class="theExpander"><span class="btnAuthorExpand"></span></th>
          </tr>
          <tr class="authRow" data-id="4" >
            <th><span><img src="img/BirnaTh.png" alt=""></span>Birna Thorkelssdóttir</th>
            <th class="statistics article desk" data-article="180">180</th>
            <th class="statistics graphs desk" data-graphs="68">68</th>
            <th class="statistics likes desk" data-likes="55">55</th>
            <th class="statistics unlikes desk" data-unlikes="18">18</th>
            <th class="statistics lang desk" data-lang="IS">IS</th>
            <th class="theExpander"><span class="btnAuthorExpand"></span></th>
          </tr>
          <tr class="authRow" data-id="5" >
            <th><span><img src="img/Smith1.png" alt=""></span>Rasmus Nielsen</th>
            <th class="statistics article desk" data-article="10">10</th>
            <th class="statistics graphs desk" data-graphs="6">6</th>
            <th class="statistics likes desk" data-likes="1">1</th>
            <th class="statistics unlikes desk" data-unlikes="5">5</th>
            <th class="statistics lang desk" data-lang="DK">DK</th>
            <th class="theExpander"><span class="btnAuthorExpand"></span></th>
          </tr>
        </tbody>
    </table>
    <div class="table-pagnation">
      <span>1 of 34</span>
    </div>
  </div>

</div>
