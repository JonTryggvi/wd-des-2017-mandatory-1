<div id="pageHeat" class="page">
  <div class="wrapper dashboard__container__headlineFilter" >
    <div class="dashboard__container__headlineFilter__text">
      <h2>Geographical Heatmap</h2>
    </div>
    <div class="dashboard__container__headlineFilter__filter">
      <div class="filterButton">
        <p>30 days</p>
        <p>60 days</p>
        <p>90 days</p>
      </div>
      <div class="filterButton_top">
        <p>30 days</p>
      </div>
    </div>
  </div>
  <div class="heatmap__container">
    <div class="heatMap">
      <img src="img/heatmap.jpg" alt="">
    </div>
  </div>

</div>
